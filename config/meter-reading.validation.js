const Joi = require('joi');

module.exports = {
  // POST /api/export/
  update: {
    body: {
      meterReadingId: Joi.number().required(),
      Status: Joi.string().required(),
      SuperID: Joi.number().required()
    }
  }
};
