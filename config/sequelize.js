const joi = require('joi');
require('dotenv').config();

// define validation for all the env vars
const envVarsSchema = joi.object({
  NODE_ENV: joi.string()
    .allow(['development', 'production', 'test', 'provision'])
    .default('development'),
  PORT: joi.number()
    .default(4041)
}).unknown()
  .required();

const {
  error,
  value: envVars
} = joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}


const config = {
  username: envVars.DB_USER,
  password: envVars.DB_PASS,
  database: envVars.DB_NAME,
  host: envVars.DB_HOST,
  dialect: 'mysql',
  underscored: true,
  dialectOptions: {
    decimalNumbers: true,
    logging: true
  },
  seederStorage: 'sequelize',
  seederStorageTableName: 'seeder_sequelize_data',
  pool: {
    max: 5,
    min: 0,
    idle: 20000,
    acquire: 40000,
    evict: 20000
  },
  logging: true,
  skipDb: envVars.DEBUG_CSV === 'true' || false
};

module.exports = config;
