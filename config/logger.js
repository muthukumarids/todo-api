const winston = require('winston');
// const config = require('config').logging;
// const dbConfig = require('config').db;
const config = require('./config');
const sequelizeWinston = require('./winston-sequelize');
const sequelizeConfig = require('./sequelize');

const env = process.env.NODE_ENV || 'development';

const options = sequelizeConfig;

winston.remove(winston.transports.Console);

if (!config.skipDb) {
  winston.add(winston.transports.Sequelize, {
    level: 'debug',
    username: options.username,
    password: options.password,
    database: options.database,
    host: options.host,
    dialect: options.dialect
  });
}

winston.add(winston.transports.Console, {
  level: config.level,
  colorize: true,
  timestamp: true,
  debugStdout: true
});


module.exports = winston;
module.exports.stream = {
  write: function (message, encoding) {
    winston.info(message);
  }
};
