const Joi = require('joi');

// require and configure dotenv, will load vars in .env in PROCESS.ENV
const envPath = process.env.NODE_ENV === 'test' ? '.env.test' : '.env';
require('dotenv').config({ path: envPath });

// define validation for all the env vars
const envVarsSchema = Joi.object({
  NODE_ENV: Joi.string()
    .allow(['development', 'production', 'test', 'provision'])
    .default('development'),
  PORT: Joi.number()
    .default(4041)
}).unknown()
  .required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  jwtSecret: envVars.JWT_SECRET,
  databaseName: envVars.DB_NAME,
  databaseUserName: envVars.DB_USER,
  databasePassword: envVars.DB_PASS,
  databasePort: envVars.DB_PORT,
  databaseHost: envVars.DB_HOST,
  pdfStoragePath: envVars.PDF_STORAGE_PATH,
  jtwExpireIn: parseInt(envVars.JTW_EXPIRE_IN, 10) || 28800,
  debugCSV: envVars.DEBUG_CSV === 'true' || false,
  skipDb: envVars.SKIPDB === 'true' || false,
  firebaseURL: envVars.FIREBASE_URL,
  useFirebase: envVars.USE_FIREBASE === 'true' || false,
  exportDir: envVars.EXPORT_DIR
};

const configSqe = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
};

// export default config;
module.exports = config;
