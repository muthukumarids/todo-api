//
// winston-sequelize.js: Transport for outputting to a database
//
const Sequelize = require('sequelize');
const util = require('util');
const winston = require('winston');
const sequelizeConfig = require('./sequelize');
const miscHelper = require('../server/helpers/misc.helper');

const env = process.env.NODE_ENV || 'development';
const seqOptions = sequelizeConfig;


//
// ### function sequelize (options)
// Constructor for the Sequelize transport object.
//
const sequelize = function (options) {
  options = options || {};

  // Winston options
  this.name = 'Sequelize';
  this.level = options.level || 'info';

  // Sequelize options
  this.dialect = options.dialect || 'mysql';
  this.host = options.host || 'localhost';
  this.port = options.port || 3306;
  this.database = options.database || 'log';
  this.username = options.username || null;
  this.password = options.password || null;
  this.omitNull = options.omitNull || false;
  this.table = options.table || 'logs';
  this.safe = options.safe || true;
  this.logging = options.logging || false;
  this.silent = options.silent || false;

  this.sequelize = new Sequelize(
    this.database,
    this.username,
    this.password, {
      dialect: this.dialect,
      host: this.host,
      port: this.port,
      omitNull: this.omitNull,
      logging: this.logging,
      define: {
        underscored: true
      }
    }
  );

  this.LogModel = this.sequelize.define('Log', {
    logId: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    level: {
      type: Sequelize.STRING(5)
    },
    msg: {
      type: Sequelize.TEXT
    },
    meta: {
      type: Sequelize.TEXT
    },
    created_at: {
      type: Sequelize.DATE
    },
    ip_address: {
      type: Sequelize.STRING(20)
    },
    session_id: {
      type: Sequelize.STRING(50)
    },
    route: {
      type: Sequelize.TEXT
    },
    query: {
      type: Sequelize.TEXT
    },
    body: {
      type: Sequelize.TEXT
    },
    host: {
      type: Sequelize.STRING(255)
    },
    url: {
      type: Sequelize.TEXT
    },
    stack_trace: {
      type: Sequelize.TEXT
    }
  }, {
    tableName: this.table,
    timestamps: false
  });
};

exports.Sequelize = sequelize;

//
// Inherit from `winston.Transport`.
//
util.inherits(sequelize, winston.Transport);

//
// Define a getter so that `winston.transports.Sequelize`
// is available and thus backwards compatible.
//
winston.transports.Sequelize = sequelize;

//
// ### function log (level, msg, [meta], callback)
// #### @level {string} Level at which to log the message.
// #### @msg {string} Message to log
// #### @meta {Object} **Optional** Additional metadata to attach
// #### @callback {function} Continuation to respond to when complete.
// Core logging method exposed to Winston. Metadata is optional.
//
sequelize.prototype.log = function (level, msg, meta, callback) {
  if (this.silent) {
    return callback(null, true);
  }

  if (typeof meta !== 'object' && meta != null) {
    meta = {
      meta: meta
    };
  }

  const body = !miscHelper.isEmptyObj(meta) ? miscHelper.getStringValue(meta.body) : null;
  const host = !miscHelper.isEmptyObj(meta) ? meta.hostname : null;
  const ip = !miscHelper.isEmptyObj(meta) ? meta.ip : null;
  let metaString = null;
  if (!miscHelper.isEmptyObj(meta)) {
    metaString = JSON.stringify(meta.meta);
  } else {
    metaString = JSON.stringify(meta);
  }
  const query = !miscHelper.isEmptyObj(meta) ? miscHelper.getStringValue(meta.query) : null;
  const route = !miscHelper.isEmptyObj(meta) ? miscHelper.getStringValue(meta.route) : null;
  const sessionId = !miscHelper.isEmptyObj(meta) ? meta.sessionID : null;
  const url = !miscHelper.isEmptyObj(meta) ? meta.url : null;
  const stackTrace = !miscHelper.isEmptyObj(meta) ? meta.stackTrace : null;

  const options = {
    level: level,
    msg: msg,
    meta: metaString,
    created_at: new Date(),
    ip_address: ip,
    session_id: sessionId,
    route: route,
    query: query,
    body: body,
    host: host,
    url: url,
    stack_trace: stackTrace
  };
  console.log(`Create Options: ${JSON.stringify(options)}`);
  return this.LogModel.create(options)
    .then(function (log) {
      return callback(log);
    })
    .catch(function (err) {
      return callback(err);
    });
};

//
// ### function close ()
// Cleans up resources (streams, event listeners) for
// this instance (if necessary).
//
sequelize.prototype.close = function () {
  this.sequelize.connectorManager.disconnect();
};
