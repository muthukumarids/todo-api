const Joi = require('joi');

module.exports = {
  // POST /api/users
  create: {
    body: {
      JobRepNo: Joi.number().required(),
      QtyInstalled: Joi.number().required(),
      WorkCode: Joi.string().required(),
      MaterialCode: Joi.string().required(),
      Description: Joi.string().required()
    }
  }
};
