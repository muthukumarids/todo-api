const express = require('express');
const morganLogger = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const compress = require('compression');
const methodOverride = require('method-override');
const cors = require('cors');
const httpStatus = require('http-status');
const expressValidation = require('express-validation');
const helmet = require('helmet');
const APIError = require('../server/helpers/APIError');
const routes = require('../server/routes/index.route');
const config = require('./config');
const middleware = require('../server/helpers/middleware');

const logger = require('./logger');

// import winstonInstance from './winston';

const app = express();

// if (config.env === 'development') {
//   app.use(morganLogger({ stream: logger.stream }));
// }

// parse body params and attache them to req.body
app.use(bodyParser.json({ limit: '100mb' }));
app.use(bodyParser.urlencoded({ extended: true, parameterLimit: 99999 }));


app.use(cookieParser());
// https://stackoverflow.com/questions/9291548/how-can-i-find-the-session-id-when-using-express-connect-and-a-session-store
app.use(session({
  secret: 'ghcc10ffc47b1e9b85571d06f5e4b406',
  resave: false,
  cookie: {
    secure: false,
    maxAge: 1000 * 60 * 60 * 24 * 7,
    domain: 'localhost'
  },
  saveUninitialized: true
}));
app.use(compress());
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet());

//  mssql enable
//  app.use()
//  enable CORS - Cross Origin Resource Sharing
app.use(cors());

// enable detailed API logging in dev env
/*  if (config.env === 'development') {
  expressWinston.requestWhitelist.push('body');
  expressWinston.responseWhitelist.push('body');
  app.use(expressWinston.logger({
    winstonInstance,
    meta: true, // optional: log meta data about request (defaults to true)
    msg: 'HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms',
    colorStatus: true // Color the status code (default green, 3XX cyan, 4XX yellow, 5XX red).
  }));Fexpress
} */
app.use(middleware.routeLogger);

// mount all routes on /api path
app.use('/api', routes);

// if error is not an instanceOf APIError, convert it.
app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    // validation error contains errors which is an array of error each containing message[]
    const unifiedErrorMessage = err.errors.map(error => error.messages.join('. ')).join(' and ');
    const error = new APIError(unifiedErrorMessage, err.status, true);
    return next(error);
  }

  if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, err.status, err.isPublic);
    return next(apiError);
  }

  return next(err);
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new APIError('API not found', httpStatus.NOT_FOUND);
  return next(err);
});

// log error in winston transports except when executing test suite
if (config.env !== 'test') {
//   app.use(expressWinston.errorLogger({
//     winstonInstance
//   }));
}

// error handler, send stacktrace only during development
app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  res.status(err.status).json({
    message: err.isPublic ? err.message : httpStatus[err.status],
    stack: config.env === 'development' ? err.stack : {}
  });
});

module.exports = app;
