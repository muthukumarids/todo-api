const Joi = require('joi');

module.exports = {
  // POST /api/users
  createUser: {
    // body: {
    //   email: Joi.string().required().email(),
    //   password: Joi.required(),
    //   role: Joi.string().valid('Admin', 'Superintendent', 'Payroll Clerk', 'Mechanic')
    // }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      email: Joi.string().required().email(),
      password: Joi.required(),
      status: Joi.string().valid('Archived', 'Invited', 'Active'),
      role: Joi.string().valid('Admin', 'Superintendent', 'Payroll Clerk', 'Mechanic')
    },
    params: {
      userId: Joi.number().integer().required()
    }
  },

  // POST /api/auth/login
  login: {
    // body: {
    //   email: Joi.string().required().email(),
    //   password: Joi.string().required()
    // }
  }
};
