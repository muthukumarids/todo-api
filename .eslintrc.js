module.exports = {
  "rules": {
    "indent": [
      2,
      2,
      {
        "SwitchCase": 1
      }
    ],
    "space-before-function-paren": [
      2,
      {
        "anonymous": "always",
        "named": "never"
      }
    ],
    "no-use-before-define": [
      2,
      "nofunc"
    ],
    "linebreak-style": 0,
    "func-names" : 0,
    "max-len" : 0,
    "object-shorthand" : 0,
    "arrow-parens" : 0,
    "no-param-reassign" : 0,
    "prefer-arrow-callback" : 0,
    "no-console" : 0,
    "no-unused-vars" : 0,
    "consistent-return" : 0,
    "arrow-body-style" : 0,
    // TODO: turn on later
    "comma-dangle": [
      0
    ],
    "import/no-extraneous-dependencies": [
      "error",
      {
        "devDependencies": true
      }
    ],
    "no-underscore-dangle": [
      0
    ]
  },
  "env": {
    "node": true,
    "mocha": true
  },
  "parserOptions": {
    "ecmaVersion": 2017,
    "sourceType": "module"
  },
  "extends": [
    "eslint:recommended",
    "airbnb-base"
  ]
}
