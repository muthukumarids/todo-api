const path = require('path');
const fs = require('fs');
const config = require('./config/config');
const app = require('./config/express');

if (['development', 'devtest'].indexOf(process.env.NODE_ENV) > -1) { // Only in dev environment
  // Absolute path to output file
  const filepath = path.join(__dirname, './out/routes.generated.txt');

  if (!fs.existsSync('../out')) {
    fs.mkdirSync('../out');
  }
  // Invoke express-print-routes
  require('express-print-routes')(app, filepath); // eslint-disable-line global-require
}

// End has to be enabled for making clustering
if (!module.parent) {
  // listen on port config.port
  console.log(`worker ${process.pid} else started`);
  app.listen(config.port, () => {
    console.info(`server started on port ${config.port} (${config.env})`); // eslint-disable-line no-console
  });
}

module.exports = app;
