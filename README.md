# Toto List #
## Developer Setup ##
### Dependencies ###

* Node.js (version: 8.11.4)
* MySQL Database (version 5.7.23+)
* Nodemon - Can be installed with just the package.json or globally

### Useful tools ###

* MySQL Workbench (6.3)
* Postman (https://www.getpostman.com/)
    * Used to make calls to the API.
* Cross-env (https://github.com/kentcdodds/cross-env#readme)
    * Used to set the environment variable before executing command. This is used so we can set NODE_ENV before running the tests.

### Getting Started ###

After cloning the git repo, open a terminal/command prompt at the root folder of the project and run `npm i`.

#### Setting a local config ####

Copy .env.exmample to .env (if it doesn't exist already)

Update the .env file with the correct information.

```js
NODE_ENV=development
PORT=4041
JWT_SECRET=4a6760e3-1945-08a6-b006-602233f77dbf
DB_NAME=todo
DB_HOST=<server ip/name>
DB_USER=<username>
DB_PASS=<password>
DB_PORT=3306
JTW_EXPIRE_IN=28800
SKIPDB=false
```

#### Setting up Database ####
Open a terminal at the root folder of the project (or command prompt)

The package.json file contains several scripts that help facilitate the setting up of the database.  After you have installed all the required packages, run the following scripts.
* "npm run createdb" - This will create the database, as defined in the config.js
* "npm run updatedb" - This will apply all the migration files (located in the .\migrations folder).
    * The SequelizeMeta table is where the applied migrations are stored.  This command will apply any migrations that haven't been applied previously.
* "npm run seeddb" - This will apply the seed files (located in the .\seeders folder).
    * The seeder_sequelize_data table is where the applied seed files are stored.  This command will apply any seeds that haven't been applied previously.
    * Information about how the seeding works can be found here: http://docs.sequelizejs.com/manual/tutorial/migrations.html

## Run the project ##
Open a terminal at the root folder of the project (or command prompt)
`npm install`

`npm run cleandb`

`npm start`

The server should be listening in the console at <http://localhost:4041>.

**Note:** *See running the project with Forever below.*


## Debugging the project with VSCODE ##

Create launch


## Useful Links
### Postman
* How to add the JWT token to an environment variable, so when you login the token is saved and can be used by other APIs
https://medium.com/@codebyjeff/using-postman-environment-variables-auth-tokens-ea9c4fe9d3d7