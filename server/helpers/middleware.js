const _ = require('lodash');
const httpStatus = require('./HTTPConstants');
const logger = require('../../config/logger');
const miscHelper = require('../helpers/misc.helper');
const config = require('../../config/config');

/**
 * Determines whether the current user is able to access the desired route.
 * @param {string[]} roles Roles to allow access
 */
const hasAccess = function (roles) {
  return function (req, res, next) {
    if (roles && req) {
      next(); // TODO: implement security checking for restrictions via req.session.user.restrictions
    } else {
      res.status(httpStatus.STAT_401_UNAUTHORIZED).send({
        message: 'User cannot perform the required action.'
      });
    }
  };
};

/**
 * Roles to exclude form accessing route.
 * @param {string[]} roles Roles to exclude from function
 */
const excludeAccess = function (roles) {
  return function (req, res, next) {
    let badUser = true;
    if (roles && req && req.user) {
      if (roles.indexOf(req.user.role) < 0) {
        badUser = false;
      }
    }
    if (!badUser) {
      next();
    } else {
      res.status(httpStatus.STAT_401_UNAUTHORIZED).send({
        message: 'User cannot perform the required action.'
      });
    }
  };
};

const routeLogger = function (req, res, next) {
  try {
    if (req && req.body && !miscHelper.isEmptyObj(req.body) && Object.prototype.hasOwnProperty.call(req.body, 'password')) {
      const reqCopy = _.cloneDeep(req);
      delete reqCopy.body.password;
      logger.info(`route called: ${req.method}:${req.path}`, reqCopy);
    } else {
      logger.info(`route called: ${req.method}:${req.path}`, req);
    }
  } catch (e) {
    //
    logger.error('Error running routeLogger middleware');
  }
  next();
};
exports.hasAccess = hasAccess;
exports.excludeAccess = excludeAccess;
exports.routeLogger = routeLogger;
