const _ = require('lodash');
const requestPromise = require('request-promise');
const logger = require('../../config/logger');
const config = require('../../config/config');

/**
 * Will remove all but includeIds from the list and sends the list along with the removeids to firebase.
 *
 * @param {string} type Data type {accountingcode, paycode, unitnumber}
 * @param {object[]} includeIds Ids to include in the mobile app
 * @param {object[]} removeIds Ids to remove from the mobile
 * @param {object[]} list List of options available
 */
module.exports.sendMobileUpdate = async (type, includeIds, removeIds, list) => {
  return new Promise((resolve, reject) => {
    logger.info(`Sending update to mobile: ${type}`, { meta: { i: includeIds, r: removeIds } });
    const sendList = list.filter((item) => {
      return item.MechanicAppYN;
    });
    const payload = sendList;
    if (['AccountingCodes', 'PayCodes', 'UnitNumbers'].indexOf(type) < 0) {
      return reject(new Error(`Type (${type} not supported)`));
    }
    // now send list
    logger.info(`Sending mobile update to mobile: ${type}`, { meta: { i: includeIds, r: removeIds, p: payload } });
    if (config.useFirebase) {
      const url = `${config.firebaseURL}/update${type}`;
      requestPromise.post({ url: url, json: payload })
        .then((result) => {
          logger.info(`sendMobileUpdate: Firebase request successful. ${JSON.stringify(result)}`);
          return resolve({ status: true, data: result });
        }).catch((e) => {
          logger.error(`sendMobileUpdate: Failed to send to firebase. ${JSON.stringify(e)}`);
          return resolve({ status: false, data: e, message: 'Error submitting rejection to firebase' });
        });
    } else {
      logger.info('sendMobileUpdate: Skipping firebase push due to settings.');
      resolve({ status: true, data: {} });
    }
  });
};
