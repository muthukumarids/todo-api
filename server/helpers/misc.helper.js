/**
 * Check to see if item is Empty
 */
module.exports.isEmpty = (data) => {
  if (typeof (data) === 'object') {
    if (JSON.stringify(data) === '{}' || JSON.stringify(data) === '[]') {
      return true;
    }
    if (!data) {
      return true;
    }
    return false;
  }
  if (typeof (data) === 'string') {
    if (!data.trim()) {
      return true;
    }
    return false;
  }

  if (typeof (data) === 'undefined') {
    return true;
  }
  return false;
};

/**
 * Check to see if object is empty
 */
module.exports.isEmptyObj = (obj) => {
  if (obj === null
    || obj === undefined
    || Array.isArray(obj)
    || typeof obj !== 'object'
  ) {
    return true;
  }
  return Object.getOwnPropertyNames(obj).length === 0;
};

// module.exports.isCyclic = function (obj) {
//   const keys = [];
//   const stack = [];
//   const stackSet = new Set();
//   let detected = false;

//   function detect(dObj, key) {
//     if (dObj && typeof dObj !== 'object') { return; }

//     if (stackSet.has(dObj)) { // it's cyclic! Print the object and its locations.
//       const oldindex = stack.indexOf(dObj);
//       const l1 = `${keys.join('.')}.${key}`;
//       const l2 = keys.slice(0, oldindex + 1).join('.');
//       // console.log('CIRCULAR: ' + l1 + ' = ' + l2 + ' = ' + obj);
//       // console.log(obj);
//       detected = true;
//       return;
//     }

//     keys.push(key);
//     stack.push(dObj);
//     stackSet.add(dObj);
//     // Object.keys()
//     // Object.keys(dObj).forEach(e => {
//     // detect(dObj[e], e);
//     // });
//     // console.log(`key=${e}  value=${obj[e]}`));
//     for (const k in dObj) { //dive on the object's children
//       if (obj.hasOwnProperty(k)) { detect(obj[k], k); }
//     }

//     keys.pop();
//     stack.pop();
//     stackSet.delete(dObj);
//   }

//   detect(obj, 'obj');
//   return detected;
// };


module.exports.getStringValue = function (obj) {
  if (obj == null) {
    return obj;
  }
  if (typeof obj === 'string') {
    return obj;
  }
  return JSON.stringify(obj);
};
