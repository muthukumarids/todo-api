const models = require('../models');
const logger = require('../../config/logger');
// Copied from server\controllers\dailyJobReportHeader.controller.js updateMachanicForm(req, res, next) {
/**
 * Update a given mechanic form.
 *  Note: Copied from server\controllers\dailyJobReportHeader.controller.js  and updated.
 */
module.exports.updateMechanicForm = (body) => {
  return new Promise(function (resolved, rejected) {
    const dataObj = body;
    dataObj.ModifedFormDate = null;
    console.log(dataObj);
    console.log('---------------------');
    models.DailyJobReportHeader.jobRepNoUniqueCheckUser(dataObj.JobRepNo, dataObj.id)
      .then(uniqueStatus => {
        console.log(`Count status -> ${uniqueStatus}`);
        return models.DailyJobReportEmpHeader.jobRepNoAndEmpIDUniqueCheck(dataObj.JobRepNo, dataObj.DailyJobReportEmpHeader[0].EmpID, dataObj.DailyJobReportEmpHeader[0].id);
      })
      .then(countEmp => {
        return models.DailyJobReportHeader.checkForemanSupertendnet(dataObj.DailyJobReportEmpHeader[0].EmpID, dataObj.SuperID);
      })
      .then(uniqueStatus => {
        return models.DailyJobReportHeader.update(dataObj, {
          where: {
            id: dataObj.id
          }
        });
      })
      .then(savedDailyJobHeader => {
        return models.DailyJobReportHeader.empHeaderUpdate(dataObj);
      })
      .then(async (savedDailyJobEmo) => {
        const result = await models.User.updateLastWorkOrder(dataObj.DailyJobReportEmpHeader[0].EmpID);
        if (!result.status) {
          // log error here
          logger.error(`Error updating last work order number: ${result.message}`);
        }

        // TODO: Do we need to save to PDF anymore?
        /**
         * // TODO Anser this?
         * removed code to save to PDF?? Do we need this? */
        models.DailyJobReportHeader.update({
          EntryVerified: 'Y'
        }, {
          where: {
            id: dataObj.id
          }
        })
          .then(verified => {
            resolved({
              status: true,
              message: models.DailyJobReportHeader.messages.updatedSuccess,
              data: body
            });
          })
          .catch(e => {
            rejected(e);
          });
      })
      .catch(e => {
        console.log('====================');
        console.log(e.name);
        console.log(e);
        let errorMessage = 'Oops!! Server error, Try again later';
        if (e.name === 'SequelizeUniqueConstraintError') {
          errorMessage = `Oops!! Unique constraint error, check your field ( ${e.SequelizeUniqueConstraintErrorField} )'`;
        }
        if (e.name === 'SequelizeForemanError') {
          errorMessage = `Oops!! you need to change Emp name which has id is 9999, check your field ( ${e.SequelizeUniqueConstraintErrorField} )`;
        }

        if (e.name === 'SequelizeSupertendentError') {
          errorMessage = `Oops!! you need to change Superintendent name which has id is 9999, check your field ' + '( ${e.SequelizeUniqueConstraintErrorField} )`;
        }

        console.log('====================');
        rejected(e);
        //  {
        //   status: false,
        //   message: errorMessage
        // });
      });
  });
};
