const request = require('supertest-as-promised');
const httpStatus = require('http-status');
const chai = require('chai');
const _ = require('lodash');
const randomWorld = require('random-world');
const {
  expect,
  assert
} = require('chai');
const {
  User,
  Employees
} = require('../models');
const app = require('../../index');
const logger = require('../../config/logger');

chai.config.includeStack = true;

const adminUser = {
  email: 'demo@microexcel.com',
  password: 'asdfasdf'
};

let jwtToken = null;
let createdUser = null;
const testUserEmail = 'jon@email.com';
let createdUsers = [];
let createdEmployees = [];
let sortList1 = {};
let sortList2 = {};
let sortList3 = {};


function cleanUsersByEmail(list) {
  return User.destroy({
    where: {
      email: {
        $in: list
      }
    }
  });
}

function cleanUsers(list) {
  const idList = _.map(list, (user) => {
    return user.userId;
  });
  return User.destroy({
    where: {
      userId: {
        $in: idList
      }
    }
  });
}

const user = {
  email: 'jon@email.com',
  firstName: 'David',
  lastName: 'Smith',
  empID: 99102,
  role: 'Superintendent',
  password: 'none',
  truckID: 'AAA',
  startingWONum: 1,
  lastWONum: 0,
  status: 'Active'
};

async function CreateDebugEmployees(count = 50) {
  const newEmployees = [];
  for (let i = 0; i < count; i += 1) {
    const employee = {
      EmpID: 75400 + i,
      LName: randomWorld.lastname(),
      FName: randomWorld.firstname(),
      MidName: randomWorld.random({
        len: 1,
        chars: 'A-Z'
      }),
      Suffix: null,
      Nickname: null,
      PRCompany: 'METEST',
      Class: randomWorld.pickone({
        items: 'MSND|150|800|LAB|OTHR|OFCR|301|NWK|FRM|NULL|SUPR|YARD|SHOP|GFRM|OFS|MS|100|ESTI'
      }),
      PRCo: '1',
      PRGroup: '2',
      PRDept: 'JOBS',
      EarnCode: '4',
      HRCo: '1',
      ActiveYN: 'Y'
    };
    newEmployees.push(employee);
  }
  try {
    await Employees.bulkCreate(newEmployees);
  } catch (e) {
    logger.debug(`Error Creating Bulk ${e.message}`);
  }
  /**
   * Find users created and the existing ones.
   */

  const existingTestEmployees = await Employees.findAll({
    where: {
      PRCompany: 'METEST'
    }
  });
  createdEmployees = createdEmployees.concat(existingTestEmployees);
  // newEmployees.forEach(async (emp) => {
  //   try {
  //     const newEmp = await Employees.create(emp);
  //     createdEmployees.push(newEmp);
  //   } catch (e) {
  //     console.log(`exception: ${e}`);
  //   }
  // });
}

// async function cleanDebugUsers() {
//   const idList = _.map(createdUsers, (u) => {
//     return u.userId;
//   });
//   return User.destroy({
//     where: {
//       userId: {
//         $in: idList
//       }
//     }
//   });
// }
async function cleanDebugEmployees() {
  const idList = _.map(createdEmployees, (employee) => {
    return employee.id;
  });
  return Employees.destroy({
    where: {
      id: {
        $in: idList
      }
    }
  });
}

async function AddDebugUsers(count = 50) {
  await CreateDebugEmployees(count + 10);
  /**
   * Find users that may already exist.
   */
  const existingTestUsers = await User.findAll({
    where: {
      lastName: {
        $like: 'ME|%'
      }
    }
  });
  createdUsers = createdUsers.concat(existingTestUsers);
  const empIDS = _.map(createdEmployees, (employee) => {
    return employee.EmpID;
  });
  const newUsers = [];
  for (let i = 0; i < count; i += 1) {
    const tempUser = {};
    tempUser.empID = empIDS[i];
    tempUser.email = randomWorld.email();
    tempUser.firstName = randomWorld.firstname();
    tempUser.lastName = `ME|${randomWorld.lastname()}`;
    tempUser.role = randomWorld.pickone({
      items: 'Admin|Superintendent|Payroll Clerk|Mechanic'
    });
    tempUser.status = randomWorld.pickone({
      items: 'Archived|Invited|Active'
    });
    tempUser.password = 'asdfasdf';

    newUsers.push(tempUser);
  }
  newUsers.forEach(async (u) => {
    const cUser = await User.create(u);
    createdUsers.push(cUser);
  });
  return true;
}

/* Login is a valid user */
before((done) => {
  (async () => {
    request(app)
      .post('/api/auth/login')
      .send(adminUser)
      .expect(httpStatus.OK)
      .then(async (res) => {
        expect(res.body).to.have.property('token');
        jwtToken = res.body.token;
        const emailList = [];
        emailList.push(testUserEmail);
        await cleanUsersByEmail(emailList);
        await AddDebugUsers();
        done();
      })
      .catch(done);
  })();
});


/**
 * root level hooks
 */
after((done) => {
  (async () => {
    // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
    try {
      await cleanUsers(createdUsers);
      await cleanDebugEmployees();
      done();
    } catch (e) {
      assert.fail(false, true, 'Failed to clean up');
      done();
    }
  })();
});

describe('## User APIs', () => {
  describe('# POST /api/users', () => {
    it('should create a new user', (done) => {
      request(app)
        .post('/api/users')
        .set('Authorization', `Bearer ${jwtToken}`)
        .send(user)
        .expect(httpStatus.OK)
        .then((res) => {
          console.log(JSON.stringify(res.body));
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data.email).to.equal(user.email);
          expect(res.body.data.role).to.equal(user.role);
          expect(res.body.data.startingWONum).to.equal(1);
          expect(res.body.data.lastWONum).to.equal(0);
          createdUser = res.body.data;
          createdUsers.push(createdUser);
          done();
        })
        .catch(done);
    });

    it('should fail when creating a user with the same empID', (done) => {
      const empidUsers = _.filter(createdUsers, (u) => {
        return u.empID !== undefined || u.empID !== null;
      });
      const newUser = {
        email: 'samsmith@email.com',
        firstName: 'Sam',
        lastName: 'ME|Smith',
        role: 'Superintendent',
        password: 'none',
        empID: empidUsers[0].empID
      };
      request(app)
        .post('/api/users')
        .set('Authorization', `Bearer ${jwtToken}`)
        .send(newUser)
        .expect(httpStatus.OK)
        .then((res) => {
          console.log(JSON.stringify(res.body));
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('message');
          expect(res.body.status).to.equal(false);
          expect(res.body.message).to.contain('Employee ID already');
          done();
        })
        .catch(done);
    });

    it('should fail when creating a user with the same email address', (done) => {
      const empidUsers = _.filter(createdUsers, (u) => {
        return u.empID !== undefined || u.empID !== null;
      });
      const newUser = {
        email: testUserEmail,
        firstName: 'Sam',
        lastName: 'ME|Smith',
        role: 'Superintendent',
        password: 'none',
        empID: empidUsers[0].empID - 1000
      };
      request(app)
        .post('/api/users')
        .set('Authorization', `Bearer ${jwtToken}`)
        .send(newUser)
        .expect(httpStatus.OK)
        .then((res) => {
          console.log(JSON.stringify(res.body));
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('message');
          expect(res.body.status).to.equal(false);
          expect(res.body.message).to.contain('Email already exists');
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /api/users/:userId', () => {
    it('should get user details', (done) => {
      request(app)
        .get(`/api/users/${createdUser.userId}`)
        .set('Authorization', `Bearer ${jwtToken}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data.email).to.equal(createdUser.email);
          expect(res.body.data.firstName).to.equal(createdUser.firstName);
          done();
        })
        .catch(done);
    });

    it('should report error with message - Not found, when user does not exists', (done) => {
      request(app)
        .get('/api/users/56c787ccc67fc16ccc1a5e92')
        .set('Authorization', `Bearer ${jwtToken}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body.message).to.equal('Not Found');
          done();
        })
        .catch(() => {
          done();
        });
    });
  });

  describe('# GET /api/users/empid/:EmpID', () => {
    it('should get user details', (done) => {
      request(app)
        .get(`/api/users/empid/${createdUser.empID}`)
        .set('Authorization', `Bearer ${jwtToken}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data.email).to.equal(createdUser.email);
          expect(res.body.data.firstName).to.equal(createdUser.firstName);
          done();
        })
        .catch(done);
    });

    it('should report error with message - Not found, when user does not exists', (done) => {
      request(app)
        .get('/api/users/56c787ccc67fc16ccc1a5e92')
        .set('Authorization', `Bearer ${jwtToken}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body.message).to.equal('Not Found');
          done();
        })
        .catch(() => {
          done();
        });
    });
  });

  describe('# PUT /api/users/:userId', () => {
    it('should update user details', (done) => {
      createdUser.lastName = 'KK';
      createdUser.role = 'Driver';
      request(app)
        .put(`/api/users/${createdUser.userId}`)
        .set('Authorization', `Bearer ${jwtToken}`)
        .send(createdUser)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data.lastName).to.equal('KK');
          expect(res.body.data.firstName).to.equal(createdUser.firstName);
          expect(res.body.data.role).to.equal('Driver');
          done();
        })
        .catch(done);
    });

    it('should update user details:empID', (done) => {
      createdUser.empID = createdEmployees[0].EmpID;
      request(app)
        .put(`/api/users/${createdUser.userId}`)
        .set('Authorization', `Bearer ${jwtToken}`)
        .send(createdUser)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data.empID).to.equal(createdEmployees[0].EmpID);
          expect(res.body.data.firstName).to.equal(createdUser.firstName);
          done();
        })
        .catch(done);
    });

    it('should update user details try 2:empID', (done) => {
      createdUser.empID = null;
      request(app)
        .put(`/api/users/${createdUser.userId}`)
        .set('Authorization', `Bearer ${jwtToken}`)
        .send(createdUser)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data.empID).to.equal(null);
          expect(res.body.data.firstName).to.equal(createdUser.firstName);
          done();
        })
        .catch(done);
    });

    it('should update user details:truckID', (done) => {
      createdUser.truckID = 'MyTruckId';
      request(app)
        .put(`/api/users/${createdUser.userId}`)
        .set('Authorization', `Bearer ${jwtToken}`)
        .send(createdUser)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data.truckID).to.equal('MyTruckId');
          expect(res.body.data.firstName).to.equal(createdUser.firstName);
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /api/users/', () => {
    it('should get all users', (done) => {
      request(app)
        .get('/api/users')
        .set('Authorization', `Bearer ${jwtToken}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data).to.have.property('rows');
          expect(res.body.data).to.have.property('count');
          expect(res.body.data.rows).to.be.an('array');
          done();
        })
        .catch(done);
    });

    it('should get all users (with pageIndex, sortDirection, and pageSize)', (done) => {
      const pageIndex = 0;
      const pageSize = 4;
      request(app)
        .get('/api/users')
        .set('Authorization', `Bearer ${jwtToken}`)
        .query({
          sortColumn: 'firstName',
          sortDirection: 'asc',
          pageIndex: pageIndex,
          pageSize: pageSize
        })
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data).to.have.property('rows');
          expect(res.body.data).to.have.property('count');
          expect(res.body.data.rows).to.be.an('array');
          expect(res.body.data.rows).to.have.length(pageSize);
          sortList1 = res.body.data;
          done();
        })
        .catch(done);
    });

    it('should get all users (with pageIndex, sortDirection, and pageSize)', (done) => {
      const pageIndex = 0;
      const pageSize = 5;
      request(app)
        .get('/api/users')
        .set('Authorization', `Bearer ${jwtToken}`)
        .query({
          sortColumn: 'email',
          sortDirection: 'asc',
          pageIndex: pageIndex,
          pageSize: pageSize
        })
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data).to.have.property('rows');
          expect(res.body.data).to.have.property('count');
          expect(res.body.data.rows).to.be.an('array');
          expect(res.body.data.rows).to.have.length(pageSize);
          sortList2 = res.body.data;
          expect(sortList2.rows[0].email).to.not.equal(sortList1.rows[0].email);
          done();
        })
        .catch(done);
    });

    it('should get all users (with pageIndex=1, sortDirection, and pageSize)', (done) => {
      const pageIndex = 1;
      const pageSize = 4;
      request(app)
        .get('/api/users')
        .set('Authorization', `Bearer ${jwtToken}`)
        .query({
          sortColumn: 'firstName',
          sortDirection: 'asc',
          pageIndex: pageIndex,
          pageSize: pageSize
        })
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data).to.have.property('rows');
          expect(res.body.data).to.have.property('count');
          expect(res.body.data.rows).to.be.an('array');
          expect(res.body.data.rows).to.have.length(pageSize);
          sortList3 = res.body.data;
          expect(sortList1.rows[0].email).to.not.equal(sortList3.rows[0].email);
          // items in sorted list 1 should not be in list 3
          const userList1 = _.map(sortList1.rows, (u) => {
            return u.userId;
          });
          const userList3 = _.map(sortList3.rows, (u) => {
            return u.userId;
          });

          const diff = userList1.filter(e => userList3.includes(e));
          expect(diff).to.have.length(0);
          done();
        })
        .catch(done);
    });

    it('#2 - should get all users (with pageIndex=1, sortDirection, and pageSize)', (done) => {
      const pageIndex = 0;
      const pageSize = 99;
      request(app)
        .get('/api/users')
        .set('Authorization', `Bearer ${jwtToken}`)
        .query({
          sortColumn: 'firstName',
          sortDirection: 'asc',
          pageIndex: pageIndex,
          pageSize: pageSize
        })
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data).to.have.property('rows');
          expect(res.body.data).to.have.property('count');
          expect(res.body.data.rows).to.be.an('array');
          expect(res.body.data.rows).to.have.length(res.body.data.count);
          done();
        })
        .catch(done);
    });

    it('should get all users test filtering (email)', (done) => {
      request(app)
        .get('/api/users')
        .set('Authorization', `Bearer ${jwtToken}`)
        .query({
          filter: 'demo@microexcel.com',
          sortDirection: 'asc',
          pageIndex: 0,
          pageSize: 5
        })
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data).to.have.property('rows');
          expect(res.body.data).to.have.property('count');
          expect(res.body.data.rows).to.be.an('array');
          expect(res.body.data.rows).to.have.length(1);
          done();
        })
        .catch(done);
    });

    it('should get all users test filtering--firstName', (done) => {
      const queryData = {
        filter: createdUsers[0].firstName
      };
      request(app)
        .get('/api/users')
        .set('Authorization', `Bearer ${jwtToken}`)
        .query(queryData)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.have.property('status');
          expect(res.body).to.have.property('data');
          expect(res.body.status).to.equal(true);
          expect(res.body.data).to.have.property('rows');
          expect(res.body.data).to.have.property('count');
          expect(res.body.data.rows).to.be.an('array');
          expect(res.body.data.rows).to.have.length.above(0);
          done();
        })
        .catch(done);
    });
  });
});

describe('# DELETE /api/users/', () => {
  it('should delete user', (done) => {
    request(app)
      .delete(`/api/users/${createdUser.userId}`)
      .set('Authorization', `Bearer ${jwtToken}`)
      .expect(httpStatus.OK)
      .then((res) => {
        expect(res.body).to.have.property('status');
        expect(res.body).to.have.property('message');
        expect(res.body).to.have.property('data');
        expect(res.body.status).to.equal(true);
        expect(res.body.data.lastName).to.equal('KK');
        expect(res.body.data.firstName).to.equal(createdUser.firstName);
        done();
      })
      .catch((e) => {
        console.log(e);
        done();
      });
  });
});
