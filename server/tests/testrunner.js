require('./auth.test');
require('./accountingCode.test');
require('./costCode.test');
require('./export.test');
require('./jobreport.test');
require('./migration.test');
require('./payCode.test');
require('./reading.test');
require('./unitNumber.test');
require('./report.test');
require('./user.test');
