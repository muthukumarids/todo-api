const path = require('path');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');
const config = require('../../config/config');
const models = require('../models');
const logger = require('../../config/logger');

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function login(req, res) {
  // Ideally you'll fetch this from the db
  logger.info(`User trying to login: ${req.body.email}`);
  models.User.loginCheck(req.body.email, req.body.password).then(response => {
    if (response) {
      const token = jwt.sign({
        firstName: response.firstName,
        status: response.status,
        role: response.role,
        email: response.email,
        userId: response.userId
      }, config.jwtSecret, { expiresIn: config.jtwExpireIn });

      logger.info(`User successfully logged in: ${req.body.email}`);
      delete response.password;

      return res.status(httpStatus.OK).json({
        status: true,
        token,
        data: response
      });
    }
    logger.info(`User failed to log in: ${req.body.email} -- response: ${response}`);
  }).catch(e => {
    console.log(e);
    logger.error('Exception during login.', e);
    return res.status(httpStatus.UNAUTHORIZED).json({
      status: false,
      message: 'Authentication error'
    });
  });
}

/**
 * This is a protected route. Will return random number only if jwt token is provided in header.
 * @param req
 * @param res
 * @returns {*}
 */
function getRandomNumber(req, res) {
  // req.user is assigned by jwt middleware if valid token is provided
  return res.json({
    user: req.user,
    num: Math.random() * 100
  });
}

module.exports.login = login;
module.exports.getRandomNumber = getRandomNumber;
