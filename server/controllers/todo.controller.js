const path = require('path');
const fs = require('fs');
const requestPromise = require('request-promise');
const httpConstants = require('../helpers/HTTPConstants');
const models = require('../models');
const miscHelper = require('../helpers/misc.helper');
const config = require('../../config/config');
const logger = require('../../config/logger');


function todoList(req, res, next) {
  try {
    models.Todos.list()
      .then((list) => {
        return res.status(httpConstants.OK_200).send({ status: true, data: list });
      }).catch((e) => {
        console.log(e);
        next(e);
      });
  } catch (e) {
    console.log('ERROR--->');
    console.log(e);
    console.log('ERROR--->');
    return res.status(httpConstants.STAT_500_INTERNAL_SERVER_ERROR).send({ status: false, message: 'Error getting list of users. ' });
  }
}

/**
 * Get user
 * @returns {todo}
 */
function getTodo(req, res) {
  if (!req.getUserFromQueryString) {
    return res.status(404).send({ status: false, message: 'Not Found' });
  }
  console.log(`Get Todo: ${req.getUserFromQueryString}`);
  return res.json({ status: true, data: req.getUserFromQueryString });
}

/**
 * Create new todo
 * @property {string} req.body.email - The email of the user
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {Todo}
 */
function create(req, res, next) {
  const todo = new models.Todos(req.body);
  todo.save()
    .then((savedTodo) => {
      res.status(httpConstants.OK_200).json({ status: true, data: savedTodo });
    })
    .catch(e => {
      next(e);
    });
}

function removeTodo(req, res, next) {
  models.Todos.update({
    status: 'Archived'
  }, { where: { id: req.params.todoId } })
    .then((savedUser) => {
      res.send({ status: true, data: savedUser, message: models.User.messages.deletedSuccess });
    })
    .catch((e) => {
      next(e);
    });
}

module.exports.todoList = todoList;
module.exports.getTodo = getTodo;
module.exports.removeTodo = removeTodo;
module.exports.create = create;
