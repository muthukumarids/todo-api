const path = require('path');
const fs = require('fs');
const requestPromise = require('request-promise');
const httpConstants = require('../helpers/HTTPConstants');
const models = require('../models');
const miscHelper = require('../helpers/misc.helper');
const config = require('../../config/config');
const logger = require('../../config/logger');

function updateFirebaseUsers() {
  return new Promise(function (resolve, reject) {

    if (config.useFirebase) {
      models.User.findAll({
        include: [
          {
            model: models.Employees,
            as: 'Employee'
          }
        ]
      }).then((users) => {
        const data = {
          users: users
        };
        // logger.info(`sendRejection: Calling Firebase with these options:${JSON.stringify(reqOptions)}`);
        // const result = await requestPromise(reqOptions);
        const url = `${config.firebaseURL}/updateMechanic`;
        requestPromise.post({ url: url, json: data })
          .then((result) => {
            logger.info(`updateFirebaseUsers: Firebase request successful. ${JSON.stringify(result)}`);
            return resolve({ status: true, data: result });
          }).catch(async (e) => {
            logger.error(`Error sending status to server. ${JSON.stringify(updateFirebaseUsers)}`);
            logger.error(`updateFirebaseUsers: Failed to send to firebase. ${JSON.stringify(e)}`);
            return resolve({ status: false, data: e, message: 'Error submitting users to firebase' });
          });
      });
    } else {
      logger.info('updateFirebaseUsers: Not submitting users to firebase');
      return resolve({ status: false, message: 'Not submitting users to firebase', skipped: true });
    }
  });
}

async function processUser(user) {
  const result = await models.User.updateLastWorkOrder(user.empID);
  // double check and update if needed
  if (result && user.lastWONum !== result.data) {
    user.lastWONum = result.data;
  }
  return user;
}
/**
 * Load user and append to req.
 */
function loadUser(req, res, next, id) {
  models.User.get(id)
    .then(async (user) => {
      if (user) {
        req.getUserFromQueryString = await processUser(user);
        return next();
      }
      if (Object.prototype.hasOwnProperty.call(req, 'getUserFromQueryString')) {
        delete req.getUserFromQueryString;
      }
      if (miscHelper.isEmpty(user)) {
        return res.status(httpConstants.STAT_404_NOT_FOUND).send({ status: false, message: 'Not Found' });
      }
      return next();
    })
    .catch(e => next(e));
}

/**
 * Load user and append to req.
 */

function loadUserByEmpID(req, res, next, id) {
  models.User.find({ where: { EmpID: id } })
    .then(async (user) => {
      if (user) {
        req.getUserFromQueryString = await processUser(user);

        // const result = await models.User.updateLastWorkOrder(user.empID);
        // // double check and update if needed
        // if (result && user.lastWONum !== result.data) {
        //   user.lastWONum = result.data;
        // }
        // req.getUserFromQueryString = user; // eslint-disable-line no-param-reassign
        return next();
      }
      if (Object.prototype.hasOwnProperty.call(req, 'getUserFromQueryString')) {
        delete req.getUserFromQueryString;
      }
      if (miscHelper.isEmpty(user)) {
        return res.status(httpConstants.STAT_404_NOT_FOUND).send({ status: false, message: 'Not Found' });
      }
      return next();
    })
    .catch(e => next(e));
}

function userList(req, res, next) {
  try {
    const {
      filter, sortDirection, sortColumn, pageIndex, pageSize
    } = req.query;
    const query = {};
    if (filter) {
      query.where = { $or: [] };
      query.where.$or.push({
        email: {
          $like: `%${filter}%`
        }
      });
      query.where.$or.push({
        firstName: {
          $like: `%${filter}%`
        }
      });
      query.where.$or.push({
        lastName: {
          $like: `%${filter}%`
        }
      });
    }
    if (pageSize) {
      query.limit = parseInt(pageSize, 10);
    }
    if (pageIndex) {
      const pSize = pageSize ? (parseInt(pageSize, 10)) : 0;
      query.offset = parseInt(pageIndex, 10) * pSize;
    }
    if (sortColumn) {
      query.order = [];
      query.order.push([sortColumn, sortDirection]);
    }
    models.User.list(query)
      .then((list) => {
        return res.status(httpConstants.OK_200).send({ status: true, data: list });
      }).catch((e) => {
        console.log(e);
        next(e);
      });
  } catch (e) {
    return res.status(httpConstants.STAT_500_INTERNAL_SERVER_ERROR).send({ status: false, message: 'Error getting list of users. ' });
  }
}

/**
 * Get user
 * @returns {User}
 */
function getUser(req, res) {
  if (!req.getUserFromQueryString) {
    return res.status(404).send({ status: false, message: 'Not Found' });
  }
  console.log(`Get User: ${req.getUserFromQueryString}`);
  return res.json({ status: true, data: req.getUserFromQueryString });
}


function updateUser(req, res, next) {
  const userData = req.getUserFromQueryString;
  const userAttributes = {};
  userAttributes.email = req.body.email;
  userAttributes.firstName = req.body.firstName;
  userAttributes.lastName = req.body.lastName;
  userAttributes.passwordToken = req.body.passwordToken;
  userAttributes.status = req.body.status;
  userAttributes.role = req.body.role;
  userAttributes.empID = req.body.empID;
  userAttributes.truckID = req.body.truckID;
  userAttributes.startingWONum = req.body.startingWONum;
  userAttributes.lastWONum = req.body.lastWONum;
  if (req.body.password) {
    userAttributes.password = req.body.password;
  }
  // user -> data from query string id // req.user. -> logged user details
  models.User.emailUniqueCheckUser(req.body.email, userData.userId)
    .then(emailCount => emailCount)
    .then(emailCount => {
      if (emailCount === 0) {
        userData.updateAttributes(userAttributes)
        // userData.save()
          .then((savedUser) => {
            updateFirebaseUsers();
            res.json({
              status: true,
              message: models.User.messages.updatedSuccess,
              data: savedUser
            });
          })
          .catch(e => {
            res.json({
              status: false,
              message: models.User.messages.serverError,
              error: e
            });
          });
      } else {
        res.json({ status: false, message: models.User.messages.emailUnique });
      }
    });
}

/**
 * Create new user
 * @property {string} req.body.email - The email of the user
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function create(req, res, next) {
  // Checking email uniquness
  // models.User.emailUniqueCheck(req.body.email)
  //   .then(emailCount => emailCount)

  models.User.empIdUniqueCheck(req.body.empID)
    .then(empIdCount => {
      return models.User.emailUniqueCheck(req.body.email);
    })
    .then(emailCount => emailCount)
    .then(emailCount => {
      if (emailCount === 0) {
        const user = new models.User(req.body);
        user.save()
          .then((savedUser) => {
            updateFirebaseUsers();
            res.status(httpConstants.OK_200).json({ status: true, data: savedUser });
          })
          .catch(e => {
            next(e);
          });
      } else {
        res.json({ status: false, message: models.User.messages.emailUnique });
      }
    })
    .catch(e => {
      let errorMessage = 'Oops!! Server error, Try again later';
      if (e.name === 'SequelizeEmpIDAlreadyUsed') {
        errorMessage = `Oops!! Employee ID already assigned to another user. (${req.body.empID})`;
        return res.status(httpConstants.OK_200).send({ status: false, message: errorMessage });
      }
      return res.status(httpConstants.STAT_401_UNAUTHORIZED).send({ status: false, message: errorMessage });
    });
}

function removeUser(req, res, next) {
  const user = req.getUserFromQueryString;
  user.status = 'Archived';

  user.update({
    status: 'Archived'
  })
    .then((savedUser) => {
      updateFirebaseUsers();
      res.send({ status: true, data: savedUser, message: models.User.messages.deletedSuccess });
    })
    .catch((e) => {
      next(e);
    });
}

module.exports.loadUser = loadUser;
module.exports.loadUserByEmpID = loadUserByEmpID;
module.exports.userList = userList;
module.exports.getUser = getUser;
module.exports.updateUser = updateUser;
module.exports.removeUser = removeUser;
module.exports.create = create;
