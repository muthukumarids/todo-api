module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('todos',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        taskName: {
          type: Sequelize.STRING
        },
        description: {
          type: Sequelize.STRING
        },
        status: {
          type: Sequelize.STRING
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('now')
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.fn('now')
        }
      })
      .then(() => {
        // perform further operations if needed
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('todos');
  }
};
