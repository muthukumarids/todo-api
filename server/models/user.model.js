const bcrypt = require('bcrypt-nodejs');
/*
exports.STATUS_ENUMS = ['Archived', 'Invited', 'Active'];
exports.ROLE_ENUMS = ['Admin', 'Superintendent', 'Payroll Clerk', 'Mechanic', 'Driver']; */

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    userId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      validate: {
        isInt: true
      }
    },
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    status: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    tableName: 'users',
    createdAt: false,
    updatedAt: false
  });

  /**
  * Preconfigured messages
  */
  User.messages = {
    loginFailed: 'Email address / Password is wrong',
    serverError: 'Server error !!!',
    updatedSuccess: 'Updated successfully',
    deletedSuccess: 'Deleted successfully',
    statusUpdate: 'Status updated successfully',
    emailUnique: 'Email already exists'

  };

  /**
   * Get a list of all the users
   */
  User.list = function (query) {
    query.attributes = {
      exclde: ['password']
    };
    return User.findAndCountAll(query);
  };

  /*
  * To get particular user and assing into load
  */
  User.get = function (userId) {
    return User.findById(userId);
  };

  /* hooks */
  User.beforeCreate(user => {
    user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(8));
  });

  /* Before update  hooks */
  User.beforeUpdate(user => {
    if (user.dataValues.password !== user._previousDataValues.password) {
      user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(8));
    }
  });

  /* Relation mapping */

  /* Login check */
  User.loginCheck = function (email, password) {
    return new Promise((resolve, reject) => {
      User.find({
        where: {
          email: email,
          status: {
            $in: ['Active', 'Invited']
          },
        },
        raw: true
      }).then(response => {
        if (response && bcrypt.compareSync(password, response.password) === true) {
          resolve(response);
        } else {
          reject(User.messages.loginFailed);
        }
      }).catch(error => {
        reject(error);
      });
    });
  };
  return User;
};
