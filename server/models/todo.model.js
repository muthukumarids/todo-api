const bcrypt = require('bcrypt-nodejs');
/*
exports.STATUS_ENUMS = ['Archived', 'Invited', 'Active'];
exports.ROLE_ENUMS = ['Admin', 'Superintendent', 'Payroll Clerk', 'Mechanic', 'Driver']; */

module.exports = (sequelize, DataTypes) => {
  const Todo = sequelize.define('Todos', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      validate: {
        isInt: true
      }
    },
    taskName: DataTypes.STRING,
    description: DataTypes.STRING,
    status: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    tableName: 'todos',
    createdAt: false,
    updatedAt: false
  });

  /**
  * Preconfigured messages
  */
  Todo.messages = {
    serverError: 'Server error !!!',
    updatedSuccess: 'Updated successfully',
    deletedSuccess: 'Deleted successfully',
    statusUpdate: 'Status updated successfully'
  };

  /**
   * Get a list of all the users
   */
  Todo.list = function () {
    const query = {};
    query.where = {
      status: 'Active'
    };
    console.log(query);
    return Todo.findAll({
      where: {
        status: 'Active'
      }
    });
  };

  /*
  * To get particular user and assing into load
  */
  Todo.get = function (todoId) {
    return Todo.findById(todoId);
  };

  return Todo;
};
