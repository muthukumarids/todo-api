const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');
const config = require('../../config/config');
const sequelizeConfig = require('../../config/sequelize');
const logger = require('../../config/logger');

const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const db = {};

const options = sequelizeConfig;

const sequelize = new Sequelize(config.databaseName, config.databaseUserName, config.databasePassword, options);

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database', err);
  });

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    console.log(file);
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
