const bcrypt = require('bcrypt-nodejs');

/**
 * Encrypt password using bcrypt before it is added
 * @param {string} password
 */
function encryptPassword(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [
      {
        userId: 1,
        firstName: 'muthu',
        lastName: 'muthu Last',
        email: 'muthu@google.com',
        password: encryptPassword('asdfasdf'),
        status: 'Active'
      }
    ]);
  },

  down: (/* queryInterface, Sequelize */) => {

  }
};
