
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('todos', [
      {
        id: 1,
        taskName: 'Frist seed task',
        description: 'This is first description for first seed',
        status: 'Active'
      }
    ]);
  },

  down: (/* queryInterface, Sequelize */) => {

  }
};
