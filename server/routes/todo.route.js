const express = require('express');
const validate = require('express-validation');
const expressJwt = require('express-jwt');
const middleware = require('../helpers/middleware');
const paramValidation = require('../../config/login-param-validation');
const todoCtrl = require('../controllers/todo.controller');
const config = require('../../config/config');


const router = express.Router(); // eslint-disable-line new-cap


router.route('/')
  .get(/* expressJwt({ secret: config.jwtSecret }), */ todoCtrl.todoList)
  .post(todoCtrl.create);

router.route('/:todoId')
  .get(/* expressJwt({ secret: config.jwtSecret }), */ todoCtrl.getTodo)
  /* .put(expressJwt({ secret: config.jwtSecret }), todoCtrl.updateTodo) */
  .delete(/* expressJwt({ secret: config.jwtSecret }), */ todoCtrl.removeTodo);

/**
 * Intercepts the :userId parameter to load the user record and place it in the
 * request object.
 * */
/* router.param('todoId', todoCtrl.loadTodo); */

module.exports = router;
