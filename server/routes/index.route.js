const express = require('express');
const userRoutes = require('./user.route');
const authRoutes = require('./auth.route');
const todoRoutes = require('./todo.route');


const router = express.Router(); // eslint-disable-line new-cap

router.use('/auth', authRoutes);
router.use('/users', userRoutes);
router.use('/todo', todoRoutes);
module.exports = router;
