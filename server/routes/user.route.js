const express = require('express');
const validate = require('express-validation');
const expressJwt = require('express-jwt');
const middleware = require('../helpers/middleware');
const paramValidation = require('../../config/login-param-validation');
const userCtrl = require('../controllers/user.controller');
const config = require('../../config/config');


const router = express.Router(); // eslint-disable-line new-cap


router.route('/')
  .get([expressJwt({ secret: config.jwtSecret }), middleware.excludeAccess(['Mechanic'])], userCtrl.userList)

  .post(expressJwt({ secret: config.jwtSecret }), userCtrl.create);

router.route('/:userId')
  .get(expressJwt({ secret: config.jwtSecret }), userCtrl.getUser)
  .put(expressJwt({ secret: config.jwtSecret }), userCtrl.updateUser)
  .delete(expressJwt({ secret: config.jwtSecret }), userCtrl.removeUser);


router.route('/empid/:empid')
  .get(expressJwt({ secret: config.jwtSecret }), userCtrl.getUser);


/**
 * Intercepts the :userId parameter to load the user record and place it in the
 * request object.
 * */
router.param('userId', userCtrl.loadUser);

/**
 * Intercepts the :empid parameter to load the user record and place it in the
 * request object.
 * */
router.param('empid', userCtrl.loadUserByEmpID);


module.exports = router;
